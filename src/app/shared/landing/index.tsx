import { Container } from "@mui/material"
import { Outlet } from "react-router-dom"


export const Landing = () => {
    return (
        <>
            <Container style={{ maxWidth: '100%', margin: '0px', padding: '0px' }}>
                <Outlet />
            </Container>
        </>
    )
}
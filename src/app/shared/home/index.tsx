import { Grid } from '@mui/material'
import './home.scss'

import logoimg from '../../assets/images/logo.svg'
import { Outlet, useNavigate } from 'react-router-dom';

import { Sidemenu } from '../sidemenu';
import { Profile } from '../viewprofile/profile';
import { Header } from '../header';

export const Home = () => {

    const navigate = useNavigate();

    const Logobox = () => {
        return (
            <div className='logo-wrap'>
                <img className='logo-img' alt='logo' src={logoimg} />
                <h2 className='logo-line'>RECRUITMENT PORTAL</h2>
            </div>
        )
    }

    return (
        <>
            <Grid container className='home-container'>
                <Grid item xs={3} sm={3} md={2} lg={2} className="side-container">
                    {Logobox()}
                    <Sidemenu />
                </Grid>
                <Grid item xs={12} sm={12} md={10} lg={10} className='main-container'>
                    <Outlet />
                </Grid>
            </Grid>
        </>
    )
}
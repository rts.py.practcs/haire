import './header.scss'
import logoimg from '../../assets/images/logo.svg'


export const Header = () => {
    return (
        <>
            <div id="header">
                <div className='logo-wrap' >
                    <img className='logo-img' alt='logo' src={logoimg} />
                    <h2 className='logo-line'>RECRUITMENT PORTAL</h2>
                </div>
            </div>
        </>
    )
}
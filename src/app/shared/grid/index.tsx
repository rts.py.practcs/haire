import '../resumes/resumes.scss'
import MenuIcon from '@mui/icons-material/Menu';
import WindowIcon from '@mui/icons-material/Window';
import { createTheme, Dialog, Divider, Grid, Pagination, ThemeProvider, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Resume from '../../assets/images/resume1.svg'
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Button, ButtonState } from "../button";
import { useDownloadFile } from '../resumes/useDownloadFile';
import { DateTime } from "luxon";
import deleteiconimg from '../../assets/images/delete icon 1.svg'
import downloadiconimg from '../../assets/images/download icon  1.svg'
import { resumesurl, resumeurl } from '../../core/service/configurls';

// download //

export const Gridview = () => {
    const [page, setpage] = useState(1)
    const [posts, setposts] = useState([])
    const [pageCount, setPageCount] = useState(1)
    const [viewbtn, setViewbtn] = useState(false)
    var [totalItems, settotalItems] = useState<any>(0)
    const [loading, setLoading] = useState(true);
    const loadpost = async () => {
        const res = await axios.get(`${resumeurl}?limit=10&offset=${totalItems}`)
        setposts(res.data.results)
        let isFloat = Number((res.data.count / 10)) === (res.data.count / 10) && (res.data.count / 10) % 1 !== 0
        setPageCount(!isFloat ? (res.data.count / 10) : parseInt((res.data.count / 10).toString().split('.')[0]) + 1)
        setLoading(false);
    }

    useEffect(() => {
        const role = localStorage.getItem("role")
        if (role == "Admin") {
            setViewbtn(true)
        }
        if (role == "user") {
            setViewbtn(false)
        }
        loadpost()
    }, [page])
    const handleChangePage = (event: any, value: any) => {

        totalItems = (value * 10) - 10;
        settotalItems(totalItems)
        loadpost()

        setpage(value);

    };
    const navigate = useNavigate();

    //////// popup


    // view resume
    const [resumeview, setResumeview] = useState({ file: '' })
    const [open, setOpen] = useState(false)
    const viewresume = async (id: any) => {
        setOpen(true);
        const response = await axios.get(`${resumesurl}/${id}`)
        setResumeview(response.data.data.file)
    }
    const handleClose = () => {
        setOpen(false);
    };

    // delete resumes
    interface Item { id: number; }
    const deleteresume = (id: any) => {
        console.log(id)
        setOpen(false);
        axios.delete(`${resumeurl}/${id}`).then(() => {
            const del = posts.filter((item: Item) => id !== item.id)
            setposts(del)
        })
    }
    ////////////// 
    const outerTheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });

    // for download
    const [buttonState, setButtonState] = useState<ButtonState>(
        ButtonState.Primary
    );
    const [showAlert, setShowAlert] = useState<boolean>(false);

    const preDownloading = () => setButtonState(ButtonState.Loading);
    const postDownloading = () => setButtonState(ButtonState.Primary);

    const onErrorDownloadFile = () => {
        setButtonState(ButtonState.Primary);
        setShowAlert(true);
        setTimeout(() => {
            setShowAlert(false);
        }, 3000);
    };

    const getFileName = () => {
        return DateTime.local().toISODate() + "_sample-file.csv";
    };

    // const downloadSampleCsvFile = () => {
    //     // throw new Error("uncomment this line to mock failure of API");
    //     return axios.get(
    //         'http://192.168.18.87:8000/api/downloadpdf/IMG20230204070728.jpg',
    //         {
    //             responseType: "blob",
    //             /* 
    //             // headers: {
    //             //   Authorization: "Bearer <token>", // add authentication information as required by the backend APIs.
    //             // },
    //             */
    //         }
    //     );
    // };
    const downloadFile = async (evt: { preventDefault: () => void; }, file: any) => {
        evt.preventDefault();
        await axios({
            url:
                `http://192.168.18.87:8000/api/downloadpdf/${file.substring(file.lastIndexOf('/') + 1)}`,
            method: "GET",
            responseType: "blob"
        }).then(response => {

            console.log(response);
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = url;
            link.href = URL.createObjectURL(response.data);
            link.download = "image.jpg";
            link.setAttribute("download", file.substring(file.lastIndexOf('/') + 1));
            document.body.appendChild(link);
            link.click();
        });
    };


    // const { ref, url, download, name } = useDownloadFile({
    //     apiDefinition: downloadSampleCsvFile,
    //     preDownloading,
    //     postDownloading,
    //     onError: onErrorDownloadFile,
    //     getFileName,
    // });


    return (
        <>

            <div className='listbox'>

                {posts && posts.map(({ id, file, thumbnail }) => (
                    <div className='resumelist' >
                        <div className='resumelist1' key={id} onClick={() => viewresume(id)}>
                            {/* <a href={url} download={name} className="hidden" ref={ref} /> */}
                            {/* {
                                thumbnail ?
                                    < img src={thumbnail} className="thumbnailpic" /> : < img src={Resume} className="thumbnailpic" />
                            } */}
                            < img src={Resume} className="thumbnailpic" />
                        </div>
                        <div className='overlay'>
                            {viewbtn ?
                                <img src={deleteiconimg} alt="deleteimg" className='deleteicon' onClick={() => deleteresume(id)} /> :
                                <img src={downloadiconimg} alt="downloadimg" className='deleteicon' onClick={(ev) => downloadFile(ev, file)} />
                            }
                        </div>
                    </div>
                ))}
            </div>
            <div className='paginationcontainer'>
                <div className='paginationbox'>
                    <ThemeProvider theme={outerTheme}>

                        <Pagination
                            page={page}
                            count={pageCount} onChange={handleChangePage} color='primary'></Pagination>
                    </ThemeProvider>
                </div>
            </div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >

                <div style={{ width: '38vw', height: '90vh', backgroundColor: '#F3F3F6', overflow: 'auto' }}>
                    <div>
                        <img style={{ height: '90vh', width: '100%', display: 'flex' }} src={resumeview.file} alt="resume" />
                    </div>
                </div>

            </Dialog>
        </>
    )

}

import { Accordion, AccordionDetails, AccordionSummary, createTheme, Input, InputAdornment, TextField, ThemeProvider, Typography } from '@mui/material';
import axios from 'axios';
import React, { useState } from 'react'
import down from '../../assets/images/down13.svg'
import '../rightsidemenu/rightsidemeny.scss'


const Rightsidemenu = () => {

    /////assesssment details///////
    const initialValues = { Name: '', Tags: '', Description: '', Question: '' }
    const [userData, setUserData] = useState({
        Name: '', Tags: '', Description: '', Question: ''
    })
    const { Name, Tags, Description, Question } = userData;
    const assessmentdetails = async (data: any) => {
        try {
            const response = await axios.post('http://192.168.18.87:8000/api/assessment', data)
            console.log(response.data)
        } catch (err) {
            console.log('err')
        }
    }
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setUserData({ ...userData, [name]: value });
    }
    const data = { Name, Tags, Description, Question }
    const save = () => {
        assessmentdetails(data)
    }
    // const [open, setOpen] = React.useState(false);
    // const handleOpen = () => {
    //     setOpen(!open);
    // };
    const inputtheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });
    return (
        <div className='fullbody'>
            <div className='createcontainer'>
                <div className='createtextcontainer'>
                    <p className='createtext'>Create Assessment</p>
                </div>
                <div className='buttoncontainer'>
                    <button className='canclebutton'>cancle</button>
                    <button className='savebutton' onClick={save}>save</button>
                </div>

            </div>
            <div className='resumecontainer'>
                <Accordion id='assessmentdetail'>
                    <AccordionSummary
                        // style={{ backgroundColor: 'red' }}
                        expandIcon={<img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <div id='assessmentdetailtext' >Assessment Details</div>
                    </AccordionSummary>
                    <AccordionDetails id='detailcontainer' >
                        <ThemeProvider theme={inputtheme}>
                            <div id='emty'>
                                <TextField value={Name}
                                    onChange={handleChange}
                                    name="Name"
                                    className='inputbox' placeholder='Enter Name' label='Assessment Name' ></TextField>
                                <TextField value={Tags}
                                    onChange={handleChange}
                                    name="Tags"
                                    className='inputbox' placeholder='Enter Tags' label='Assessment Tag'></TextField>

                            </div>
                            <div id='emty1'>
                                <TextField value={Description}
                                    className='inputbox1'
                                    onChange={handleChange}
                                    name="Description"
                                    placeholder='Enter Description'
                                    label='Assessment Description'></TextField>
                            </div>
                        </ThemeProvider>

                    </AccordionDetails>


                </Accordion>

                <Accordion id='assessmentdetail'>
                    <AccordionSummary
                        // style={{ backgroundColor: 'red' }}
                        expandIcon={<img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <div id='assessmentdetailtext' >Question</div>
                    </AccordionSummary>
                    <AccordionDetails id='question' >
                        <TextField style={{ height: '100%', width: '100%', }}
                            id="filled-textarea"
                            // label="Multiline Placeholder"
                            // placeholder="Placeholder"
                            multiline
                            variant='filled'
                            value={Question}
                            name='Question'
                            onChange={handleChange}

                        />

                    </AccordionDetails>


                </Accordion>
            </div>
        </div>
    )
}
export default Rightsidemenu;

import './resumes.scss'
import MenuIcon from '@mui/icons-material/Menu';
import WindowIcon from '@mui/icons-material/Window';
import { Autocomplete, Button, Chip, createTheme, Dialog, Divider, Grid, Input, InputAdornment, Pagination, TextField, ThemeProvider, Typography } from '@mui/material';
import { Outlet, useNavigate } from 'react-router-dom';
import Resume from '../../../assets/images/resume1.svg'
import { SetStateAction, useEffect, useState } from 'react';
import axios from 'axios';
import { Gridview } from '../grid';
import { Header1 } from '../header1';
import down from '../../assets/images/down.svg'
import { resumesurl, resumeurl } from '../../core/service/configurls';
import { off } from 'process';
import React from 'react';

export const Resumes = () => {
    const [page, setpage] = useState(2)
    const [posts, setposts] = useState([])
    const [viewbtn, setViewbtn] = useState(false)
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };
    // const loadpost = async () => {
    //     const res = await axios.get(`${resumesurl}`)
    //     setposts(res.data)
    // }
    useEffect(() => {
        const role = localStorage.getItem("role")
        if (role == "Admin") {
            setViewbtn(true)
        }
        if (role == "user") {
            setViewbtn(false)
        }
        // loadpost()
    }, [])
    const navigate = useNavigate();

    // add resume
    const [openadd, setOpenadd] = useState(false)
    const Addresume = () => {
        setOpenadd(true)
    }

    const handleClose = () => {
        setOpenadd(false)
    }

    // themes
    const inputtheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });
    // FILE UPLOAD
    const top100Films = [
        ''
    ]

    //////////////////////////////////////////////////////////autocomplete////////////////////// 
    ////////////File upload//////////////////////////////////
    const [fileList, setFileList] = useState<any>(null);
    const [location, setLocation] = useState<any>([])
    const [experience, setExperience] = useState<any>([])
    const [company, setCompany] = useState<any>([])
    const [designation, setDesignation] = useState<any>([])
    const [fileNameList, setFileNameList] = useState<any>([])
    const handlelocationChange = (e: any, newLocation: any) => { setLocation(newLocation); }
    const handleexperienceChange = (e: any, newexperience: any) => { setExperience(newexperience); }
    const handlecompanyChange = (e: any, newcompany: any) => { setCompany(newcompany); }
    const handledesignationChange = (e: any, newdesignation: any) => { setDesignation(newdesignation); }

    const handleFileChange = (e: any) => {
        setFileList(e.target.files[0]);
        setFileNameList(Array.from(e.target.files!))
    }

    const handleUploadClick = () => {

        if (!fileList) {
            return;
        }

        // 👇 Create new FormData object and append files
        const bodyformdata = new FormData();
        bodyformdata.append('file', fileList)
        bodyformdata.append('location', location)
        bodyformdata.append('experience', experience)
        bodyformdata.append('company', company)
        bodyformdata.append('designation', designation)

        console.log(bodyformdata)
        // 👇 Uploading the files using the fetch API to the server
        axios.post(`${resumeurl}`, bodyformdata, {

            headers: {
                "Content-Type": "multipart/form-data",

            },

        })
            .then((res) => {
                if (res.data.status == true) {
                    setOpenadd(false)
                    console.log(bodyformdata)
                }
            })
            .catch((err) => {
                console.log(err)
            })

    };
    // const files = fileList ? [...fileList] : [];

    return (
        <>
            <Header1 />
            <div className='resumes-page-wrap'>
                <div className="searchbox">
                    <div className='resumestextbox'>
                        <p className='resumestext'>RESUMES</p>
                    </div>
                    <div className='searchtextboxadd'>
                        <div className="searchbox1add" >
                            <Input
                                disableUnderline
                                placeholder="SEARCH BY NAME"
                                id="input-with-icon-adornment"
                                className="searchadd"
                                // value={searchValue}
                                // onChange={handleChangeFilter}
                                endAdornment={
                                    <InputAdornment position="end" onClick={handleOpen} style={{ height: '10px', width: '20px', }}>
                                        <img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>
                                    </InputAdornment>
                                }
                            />
                        </div>
                        {open ? (
                            <div className="menu">
                                <input className="insidedropdown" placeholder="SEARCH BY COMPANY">
                                </input>
                                <input className="insidedropdown" placeholder="SEARCH BY EMAIL">
                                </input>
                            </div>
                        ) : null}
                    </div>
                    {viewbtn ?
                        <div className='buttonbox'>
                            <button className='button' onClick={Addresume}>
                                <p className='buttontext'>Add</p>
                            </button>
                        </div>
                        : null
                    }
                </div>
                <div className='pageviewbox'>
                    <div className='pageviewbutton'>
                        <div className='pageviewbuttonbox'>
                            <button className='view-btn' style={{ border: 'none', backgroundColor: 'rgba(255, 255, 255, 1)', cursor: 'pointer' }} onClick={() => navigate('/home/resumes/grid')}>
                                <WindowIcon sx={{ color: 'rgba(0, 141, 150, 1)', fontSize: 26 }}></WindowIcon>
                            </button>
                            <Divider orientation="vertical" variant="middle" flexItem sx={{ color: 'rgba(0, 141, 150, 1)', fontSize: 18 }} />
                            <button className='view-btn' style={{ border: 'none', backgroundColor: 'rgba(255, 255, 255, 1)', cursor: 'pointer' }} onClick={() => navigate('/home/resumes/list')}>
                                <MenuIcon sx={{ color: 'rgba(0, 141, 150, 1)', fontSize: 26 }}></MenuIcon>
                            </button>
                        </div>
                    </div>
                </div>
                <div className='listcontainer'>
                    <Outlet />
                </div>
            </div>
            <Dialog
                open={openadd}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <div className='addresume'>
                    <div className='addresumetextbox'>
                        <p className='addresumetext'>Add Resume</p>
                    </div>
                    <div className='uploadcontainer'>
                        <div className='uploadbox'>
                            <button className='button'>
                                <p className='buttontext'>Upload Resume</p>
                            </button>
                        </div>

                    </div>
                    {/* <Button style={{ marginTop: '3%', textTransform: 'none' }} variant="contained" component="label">
                        Choose File
                        <input accept="application/*, image/*" hidden multiple type="file" onChange={handleFileChange} />
                    </Button>
                    {fileNameList.length > 0 && (
                        <ul>
                            {fileNameList.map((file: any) => (
                                <li key={file.name}>{file.name}</li>
                            ))}
                        </ul>
                    )}
                    <div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: "space-evenly" }}>
                        <Autocomplete
                            className='taginput'
                            multiple
                            id="tags-filled"
                            onChange={handlelocationChange}
                            value={location}
                            options={top100Films}
                            // defaultValue={[top100Films[1].title]}
                            freeSolo
                            renderTags={(value: readonly string[], getTagProps) =>
                                value.map((option: string, index: number) => (
                                    <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                                ))
                            }
                            renderInput={(params) => (
                                <TextField

                                    {...params}
                                    variant="outlined"
                                    label="Location"
                                    placeholder="Enter the location"
                                    inputProps={{
                                        ...params.inputProps,
                                        autoComplete: 'disabled', // disable autocomplete and autofill
                                    }}
                                />
                            )}
                        />
                        <Autocomplete
                            className='taginput'
                            multiple
                            id="tags-filled"
                            onChange={handleexperienceChange}
                            value={experience}
                            options={top100Films}
                            // defaultValue={[top100Films[1].title]}
                            freeSolo
                            renderTags={(value: readonly string[], getTagProps) =>
                                value.map((option: string, index: number) => (
                                    <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                                ))
                            }
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    variant="outlined"
                                    label="Experience"
                                    placeholder="Favorites"
                                    inputProps={{
                                        ...params.inputProps,
                                        autoComplete: 'disabled', // disable autocomplete and autofill
                                    }}
                                />
                            )}
                        />
                    </div>
                    <div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: "space-evenly" }}>
                        <Autocomplete
                            className='taginput'
                            multiple
                            id="tags-filled"
                            options={top100Films}
                            onChange={handlecompanyChange}
                            value={company}
                            // defaultValue={[top100Films[1].title]}
                            freeSolo
                            renderTags={(value: readonly string[], getTagProps) =>
                                value.map((option: string, index: number) => (
                                    <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                                ))
                            }
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    variant="outlined"
                                    label="Company"
                                    placeholder="Enter the Company"
                                    inputProps={{
                                        ...params.inputProps,
                                        autoComplete: 'disabled', // disable autocomplete and autofill
                                    }}
                                />
                            )}
                        />
                        <Autocomplete
                            className='taginput'
                            sx={{ marginBottom: '20px' }}
                            multiple
                            id="tags-filled"
                            value={designation}
                            onChange={handledesignationChange}
                            options={top100Films}
                            freeSolo
                            renderTags={(value: readonly string[], getTagProps) =>
                                value.map((option: string, index: number) => (
                                    <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                                ))
                            }
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    variant="outlined"
                                    label="Designation"
                                    placeholder="Enter the Designation"
                                    inputProps={{
                                        ...params.inputProps,
                                        autoComplete: 'disabled', // disable autocomplete and autofill
                                    }}
                                />
                            )}
                        />
                    </div>
                    <Button disabled={!fileList} style={{ marginTop: '3%', textTransform: 'none' }} variant="contained" onClick={handleUploadClick}>Upload</Button> */}
                </div>
            </Dialog>
        </>
    )
}
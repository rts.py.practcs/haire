import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { Form } from "semantic-ui-react";
import { User } from "../users";
import './row.scss'

declare interface RowProps extends User {
  isChecked?: boolean;
  selectionHandler: (id: number) => void;
  deleteHandler: (id: number) => void;
  editHandler: (newUser: User) => void;
}

const Row: React.FC<RowProps> = ({
  name,
  email,
  role,
  isChecked = false,
  id,
  deleteHandler,
  editHandler
}) => {
  const [update, setUpdate] = useState(false);
  const [nameText, setNameText] = useState(name);
  const [emailText, setEmailText] = useState(email);
  const [roleText, setRoleText] = useState(role);

  //  Edit Profile 
  const updateClose = () => setUpdate(false);
  const updateShow = () => setUpdate(true);

  return (
    <div className="full-container">
      <div className={`row-container ${isChecked ? "checked" : ""}`}>
        <div className="name">

          <div> <Modal show={update} onHide={updateClose}>
            <Modal.Header closeButton>
              <Modal.Title className='user'>Edit User</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <center>
                  <Form.Field>
                    <input type="text" value={nameText}
                      onChange={(e) => setNameText(e.target.value)} placeholder='Enter User Name *' />
                  </Form.Field><br />
                  <Form.Field>
                    <input type="text" value={emailText}
                      onChange={(e) => setEmailText(e.target.value)} placeholder='Enter Email Id *' />
                  </Form.Field><br />
                  <Form.Field>
                    <input type="text" value={roleText}
                      onChange={(e) => setRoleText(e.target.value)} placeholder='Enter Company Name *' />
                  </Form.Field><br />
                  {/* <Form.Field>
    <input type="text" placeholder='Enter Contact No *'/>
    </Form.Field><br />
    <Form.Field>
    <input type="text" placeholder='Enter Address'/> 
    </Form.Field><br /> */}
                </center>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={() => {
                editHandler({
                  isChecked,
                  id,
                  name: nameText,
                  email: emailText,
                  role: roleText,

                });
                setUpdate(false);
              }}>
                Save
              </Button>
              <Button className='add' variant="primary" onClick={() => setUpdate(false)} >
                Cancel
              </Button>
            </Modal.Footer>
          </Modal></div>

          {update ? (<></>) : (name)}</div>
        <div> {update ? (<></>) : (email)}</div>
        <div>{update ? (<></>) : (role)}</div>
        <div className="actions">
          {update ? (<></>) : (
            <>
              <a onClick={() => setUpdate(true)}  >Edit</a>
              <a onClick={() => deleteHandler(id)} >Delete</a>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Row;

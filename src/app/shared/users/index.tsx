import React, { ChangeEvent, useEffect, useState } from "react";
import axios from "axios";
import './users.scss'
import { Alert, CircularProgress, Container, createTheme, Dialog, Input, InputAdornment, Pagination, Snackbar, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, ThemeProvider } from "@mui/material";
import down from '../../assets/images/down.svg'
import { Header1 } from "../header1";
import { Userservice } from "../../core/service/users";
import { userurl } from "../../core/service/configurls";
import { useParams } from "react-router-dom";

export interface User {
    id: number;
    name: string;
    email: string;
    role: string;
    isChecked?: boolean;
}

export const Users = () => {

    // Search Dropdown
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };

    // pagination theme
    const outerTheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });

    //add user
    const [add, setadd] = React.useState(false);
    const handleClickadd = () => {
        setadd(true);
    };

    const handleCloseadd = () => {
        setadd(false);
    };

    //edit user///
    const [edituser, setEdituser] = useState(
        { UserName: '', CompanyName: '', Email: '', MobileNumber: '', Address: '', City: '', State: '', Credits: '' }
    )

    const [edit, setedit] = React.useState(false);

    const [userid1, setUserid1] = useState('')

    const handleClickedit = async (id: any) => {
        setedit(true);
        const response = await axios.get(`${userurl}/${id}`)
        setEdituser(response.data)
        setUserid1(response.data.id)
    };
    const editChange = (evt: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = evt.target;
        setEdituser({ ...edituser, [name]: value })
    }

    const { editUser } = Userservice()

    async function handleEdituser(evt: any) {
        evt.preventDefault()
        const { UserName, CompanyName, Email, MobileNumber, Address, City, State, Credits } = edituser
        const editData = { UserName, CompanyName, Email, MobileNumber, Address, City, State, Credits }
        editUser(userid1, editData)
        setedit(false);
    }

    interface Item { id: number; }

    const [deleteuserdetail, setDeleteuserdetail] = useState({
        UserName: ''
    })

    // const deletechange = (evt: ChangeEvent<HTMLInputElement>) => {
    //     const { name, value } = evt.target;
    //     setDeleteuserdetail({ ...deleteuserdetail, [name]: value })
    // }

    const deleteuser = async (id: any) => {
        setDeleteact(true);
        const response = axios.get(`${userurl}/${id}`)
        setUserid1((await response).data.id)
        setDeleteuserdetail((await response).data)
    }

    const confirmdeleteuser = async () => {
        const id: any = userid1;
        axios.delete(`${userurl}/update/${id}`).then(() => {
            const del = posts.filter((item: Item) => id !== item.id)
            setposts(del)
        })
        setDeleteact(false);
    }

    const handleCloseedit = () => {
        setedit(false);
    };

    // delete account
    const [deleteact, setDeleteact] = React.useState(false)
    const handleClickdelete = () => {
        setDeleteact(true);
    }
    const handleClosedeleteact = () => {
        setDeleteact(false);
    }

    // pagination
    const [page, setpage] = useState<any>(1)
    const [posts, setposts] = useState<any[]>([])
    const [searchValue, setSearchValue] = useState("");
    const [loading, setLoading] = useState(true);

    var [totalItems, setTotalItems] = useState<any>(0);




    const loadpost = async () => {
        const res = await axios.get(`http://192.168.18.87:8000/api/recruiters?limit=10&offset=${totalItems}&search=${searchValue}`)
        const data = await res.data.results
        let isFloat = Number((res.data.count / 10)) === (res.data.count / 10) && (res.data.count / 10) % 1 !== 0
        setPageCount(!isFloat ? (res.data.count / 10) : parseInt((res.data.count / 10).toString().split('.')[0]) + 1)
        setposts(data);
        setLoading(false);

        console.log(data)
    }

    useEffect(() => {
        loadpost()
    }, [searchValue])
    // const totalPagesw: any = () => {
    //     setpage(page + 1)
    // };
    const [pageCount, setPageCount] = useState(1)



    // const stopindex = posts.slice(startIndex, totalPagesw)
    // users 
    const { addUser } = Userservice();

    const initialValues = { UserName: '', CompanyName: '', Email: '', MobileNumber: '', Address: '', City: '', State: '', Credits: '' }
    const [userData, setUserData] = useState(initialValues)
    const { UserName, CompanyName, Email, MobileNumber, Address, City, State, Credits } = userData;

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setUserData({ ...userData, [name]: value });
    }

    const data = { UserName, CompanyName, Email, MobileNumber, Address, City, State, Credits }
    const submitUser = () => {
        addUser(data)
        setadd(false);
    }

    // themes
    const inputtheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });
    /////////////////////////add user////////
    const nameformat = RegExp(/^[a-z]/)
    const mobilnumber = RegExp(/^[6-9]\d{9}$/gi)
    const emailpatten = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)
    const creditformat = RegExp(/^[[0-9]{2}$/)
    const [errormessage, seterrormessage] = React.useState<any>({})
    const handleerror = (err: any, pops: any) => {
        seterrormessage((prev: any) => ({ ...prev, [pops]: err }))
    }
    const handleblur = (value: any) => {
        if (value == 'UserName') {
            if (!userData.UserName) {
                handleerror('Enter Your Name', 'UserName')
            } else if (!userData.UserName.match(nameformat)) {
                handleerror('First Letter Capital Above Small Letter', 'UserName')
            }
        }
        if (value == 'CompanyName') {
            if (!userData.CompanyName) {
                handleerror('Enter Your CompanyName', 'CompanyName')
            }
        }
        if (value == 'Email') {
            if (!userData.Email) {
                handleerror('Enter Your Name', 'Email')
            } else if (!userData.Email.match(emailpatten)) {
                handleerror('Enter The Valid Email Id', 'Email')
            }
        }
        if (value == 'MobileNumber') {
            if (!userData.MobileNumber) {
                handleerror('Enter Your Phone Number', 'MobileNumber')
            } else if (!userData.MobileNumber.match(mobilnumber)) {
                handleerror("Enter The Valid PhoneNumber", "MobileNumber")
            }
        }
        if (value == 'Address') {
            if (!userData.Address) {
                handleerror('Enter Your Address', 'Address')
            }
        }
        if (value == 'City') {
            if (!userData.City) {
                handleerror('Enter Your City', "City")
            }
        }
        if (value == 'State') {
            if (!userData.State) {
                handleerror('Enter Your State', 'State')
            }
        }
        if (value == 'Credits') {
            if (!userData.Credits) {
                handleerror('Enter Your Credits', 'Credits')
            } else if (!userData.Credits.match(creditformat)) {
                handleerror('Enter Credit  2 Digit ', 'Credits')
            }
        }
    }
    ///////Edit user//////////////////
    const [EditErrormessage, setEditErrormessage] = React.useState<any>({})
    const handleEditusererror = (err: any, pops: any) => {
        setEditErrormessage((prev: any) => ({ ...prev, [pops]: err }))
    }
    const handleEdituserBlur = (value: any) => {
        if (value == 'UserName') {
            if (!edituser.UserName) {
                handleEditusererror('Enter Your Name', 'UserName')
            } else if (!edituser.UserName.match(nameformat)) {
                handleEditusererror('Enter your Name First Letter Capital', 'UserName')
            }
        }
        if (value == 'CompanyName') {
            if (!edituser.CompanyName) {
                handleEditusererror('Enter Your CompanyName', 'CompanyName')
            }
        }
        if (value == 'Email') {
            if (!edituser.Email) {
                handleEditusererror('Enter Your Name', 'Email')
            } else if (!edituser.Email.match(emailpatten)) {
                handleEditusererror('Enter The Valid Email Id', 'Email')
            }
        }
        if (value == 'MobileNumber') {
            if (!edituser.MobileNumber) {
                handleEditusererror('Enter Your Phone Number', 'MobileNumber')
            } else if (!edituser.MobileNumber.match(mobilnumber)) {
                handleEditusererror("Enter Valid Phone Number", "MobileNumber")
            }
        }
        if (value == 'Address') {
            if (!edituser.Address) {
                handleEditusererror('Enter Your Address', 'Address')
            }
        }
        if (value == 'City') {
            if (!edituser.City) {
                handleEditusererror('Enter Your City', "City")
            }
        }
        if (value == 'State') {
            if (!edituser.State) {
                handleEditusererror('Enter Your State', 'State')
            }
        }
        if (value == 'Credits') {
            if (!edituser.Credits) {
                handleEditusererror('Enter Your Credits', 'Credits')
            } else if (!edituser.Credits.match(creditformat)) {
                handleEditusererror('Enter Credit  2 Digit ', 'Credits')
            }
        }

    }
    const handleChangePage = (event: any, value: any) => {
        // setpage(page + 1)
        totalItems = (value * 10) - 10;
        setTotalItems(totalItems)
        loadpost()
        // setpage(value - 1);
        setpage(value);
        // console.log(value)
        // setTotalItems(totalItems + 10)
    };
    const handleChangeFilter = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setSearchValue(event.target.value);
    };
    const [open2, setOpen2] = React.useState(false);

    const handleClick = () => {
        setOpen2(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen2(false);
    };

    return (
        <>
            {/* <Snackbar open={open2} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    This is a success message!
                </Alert>
            </Snackbar> */}
            <Header1 />
            <div className="users-wrap">
                <div className="searchboxadd">
                    <div className='resumestextboxadd'>
                        <p className='resumestextadd'>USERS</p>
                    </div>
                    <div className='searchtextboxadd'>
                        <div className="searchbox1add" >
                            <Input
                                disableUnderline
                                placeholder="SEARCH BY NAME"
                                id="input-with-icon-adornment"
                                className="searchadd"
                                value={searchValue}
                                onChange={handleChangeFilter}
                                endAdornment={
                                    <InputAdornment position="end" onClick={handleOpen} style={{ height: '10px', width: '20px', }}>
                                        <img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>
                                    </InputAdornment>
                                }
                            />
                        </div>
                        {open ? (
                            <div className="menu">
                                <input className="insidedropdown" placeholder="SEARCH BY COMPANY">
                                </input>
                                <input className="insidedropdown" placeholder="SEARCH BY EMAIL">
                                </input>
                            </div>
                        ) : null}
                    </div>
                    <div className='buttonboxadd'>
                        <button className='buttonadd' onClick={() => handleClickadd()}>
                            <p className='buttontextadd'>Add</p>
                        </button>
                    </div>
                </div>
                <div className='listcontainer'>
                    <div id='container2'>
                        {loading ? (
                            <ThemeProvider theme={outerTheme}>
                                <CircularProgress color="primary" />
                            </ThemeProvider>
                        ) :
                            <TableContainer className="contain-table" sx={{ width: '95%', minHeight: '90%', backgroundColor: 'rgba(255, 255, 255, 1)', borderRadius: '15px', overflow: 'auto' }}>
                                <Table sx={{ minWidth: 650, border: 'none', overflow: 'hidden' }} aria-label="simple table">
                                    <TableHead style={{ backgroundColor: 'white' }}>
                                        <TableRow sx={{ borderBottom: '1px solid black' }}>
                                            <TableCell className="table-font" sx={{ fontSize: '0.9em', fontWeight: '700', color: '#008D96', backgroundColor: 'white' }} align="left">Name</TableCell>
                                            <TableCell className="table-font" sx={{ fontSize: '0.9em', fontWeight: '700', color: '#008D96', backgroundColor: 'white' }} align="left">Email</TableCell>
                                            <TableCell className="table-font" sx={{ fontSize: '0.9em', fontWeight: '700', color: '#008D96', backgroundColor: 'white' }} align="left">Company</TableCell>
                                            <TableCell className="table-font" sx={{ fontSize: '0.9em', fontWeight: '700', color: '#008D96', backgroundColor: 'white' }} align="left">Actions</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody >
                                        {
                                            posts.map(({ id, UserName, Email, CompanyName }) => (
                                                <TableRow key={id} className='font-wrap' style={{ backgroundColor: 'white', textAlign: 'left', border: 'none' }}>
                                                    <TableCell className="table-font1" sx={{ fontSize: '0.8em', fontWeight: '700', color: '#989898', backgroundColor: 'white' }} align="left">{UserName}</TableCell>
                                                    <TableCell className="table-font1" sx={{ fontSize: '0.8em', fontWeight: '700', color: '#989898', backgroundColor: 'white' }} align="left">{Email}</TableCell>
                                                    <TableCell className="table-font1" sx={{ fontSize: '0.8em', fontWeight: '700', color: '#989898', backgroundColor: 'white' }} align="left">{CompanyName}</TableCell>
                                                    <TableCell className="table-font1" sx={{ fontSize: '0.8em', fontWeight: '700', color: '#989898', backgroundColor: 'white' }} align="left"><a className='acion-btn' onClick={() => handleClickedit(id)}>Edit</a> <a className='acion-btn' onClick={() => deleteuser(id)}>Delete</a></TableCell>
                                                </TableRow>
                                            ))
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        }
                    </div>
                    <div className='paginationcontainer'>
                        <div className='paginationbox'>
                            <ThemeProvider theme={outerTheme}>
                                <Pagination page={page}

                                    count={pageCount} onChange={handleChangePage} color='primary'></Pagination>
                            </ThemeProvider>

                        </div>
                    </div>
                </div>
            </div>

            {/* add users */}
            <Dialog
                open={add}
                onClose={handleCloseadd}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div style={{ width: '600px', height: '455px', backgroundColor: '#F3F3F6' }}>
                    <div className='password3'>
                        <div className='space1'></div>
                        <div className='text1'>
                            <p className='word1a'>Add User</p>
                        </div>
                    </div>
                    <div className='inputcontainer2a' >
                        <ThemeProvider theme={inputtheme}>
                            <div className='inputboxnamerow'>
                                <div className='input-wrap'>
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange}
                                            onBlur={(e) => handleblur('UserName')}
                                            onFocus={(e) => handleerror(null, 'UserName')}
                                            className='inputbox' placeholder='Enter User Name' focused id="outlined-basic" label="User Name" variant="outlined" name="UserName" value={UserName} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.UserName ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.UserName}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField
                                            onBlur={(e) => handleblur("CompanyName")}
                                            onFocus={(e) => handleerror(null, 'CompanyName')}
                                            onChange={handleChange} className='inputbox' placeholder='Enter Company Name' focused id="outlined-basic" label="Company Name" variant="outlined" name="CompanyName" value={CompanyName} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.CompanyName ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.CompanyName}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField
                                            onBlur={(e) => handleblur('Email')}
                                            onFocus={(e) => handleerror(null, 'Email')}
                                            onChange={handleChange} className='inputbox' placeholder='Enter Mail Id' focused id="outlined-basic" label="Mail Id" variant="outlined" name="Email" value={Email} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.Email ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.Email}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange}
                                            onBlur={(e) => handleblur("MobileNumber")}
                                            onFocus={(e) => handleerror(null, 'MobileNumber')}
                                            className='inputbox' placeholder='Enter Contact No' focused id="outlined-basic" label="Contact No" variant="outlined" name="MobileNumber" value={MobileNumber} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.MobileNumber ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.MobileNumber}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange} className='inputbox'
                                            onBlur={(e) => handleblur("Address")}
                                            onFocus={(e) => handleerror(null, 'Address')}
                                            placeholder='Enter Address' focused id="outlined-basic" label="Address" variant="outlined" name="Address" value={Address} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.Address ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.Address}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange} className='inputbox'
                                            onBlur={(e) => handleblur("City")}
                                            onFocus={(e) => handleerror(null, "City")}
                                            placeholder='Enter City' focused id="outlined-basic" label="City" variant="outlined" name="City" value={City} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.City ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.City}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange}
                                            onBlur={(e) => handleblur("State")}
                                            onFocus={(e) => handleerror(null, 'State')}
                                            className='inputbox' placeholder='Enter State' focused id="outlined-basic" label="State" variant="outlined" name="State" value={State} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.State ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.State}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={handleChange}
                                            onBlur={(e) => handleblur("Credits")}
                                            onFocus={(e) => handleerror(null, 'Credits')}
                                            className='inputbox' placeholder='Enter Credits' focused id="outlined-basic" label="Credits" variant="outlined" name="Credits" value={Credits} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {errormessage.Credits ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{errormessage.Credits}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ThemeProvider>
                    </div>

                    <div className='buttoncontainer1'>
                        <div className='buttongap1'></div>
                        <div className='buttonbox1'>
                            <button className='button1a' onClick={handleCloseadd}><p className='button1atext'>Cancel</p></button>
                            <button className='button2a' onClick={submitUser}><p className='button2atext'>Save</p></button>
                        </div>
                    </div>
                </div>

            </Dialog>
            {/* add users end */}

            {/* edit users */}
            <Dialog
                open={edit}
                onClose={handleCloseedit}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div style={{ width: '600px', height: '455px', backgroundColor: '#F3F3F6' }}>
                    <div className='password3'>
                        <div className='space1'></div>
                        <div className='text1'>
                            <p className='word1a'>Edit User</p>
                        </div>
                    </div>

                    <div className='inputcontainer2a' >
                        <ThemeProvider theme={inputtheme}>
                            <div className='inputboxnamerow'>
                                <div className='input-wrap'>
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleEdituserBlur('UserName')}
                                            onFocus={(e) => handleEditusererror(null, 'UserName')}
                                            className='inputbox' placeholder='Enter User Name' focused id="outlined-basic" label="User Name" variant="outlined" name="UserName" value={edituser.UserName} color='primary' />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.UserName ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.UserName}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            className='inputbox'
                                            onBlur={(e) => handleEdituserBlur('CompanyName')}
                                            onFocus={(e) => handleEditusererror(null, 'CompanyName')}
                                            placeholder='Enter Company Name'
                                            focused id="outlined-basic" label="Company Name"
                                            variant="outlined" name="CompanyName"
                                            value={edituser.CompanyName}
                                            color='primary' />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.CompanyName ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.CompanyName}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleEdituserBlur('Email')}
                                            onFocus={(e) => handleEditusererror(null, 'Email')}
                                            className='inputbox' placeholder='Enter Mail Id' focused id="outlined-basic" label="Mail Id" variant="outlined" name="Email" value={edituser.Email} color='primary' />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.Email ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.Email}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleEdituserBlur('MobileNumber')}
                                            onFocus={(e) => handleEditusererror(null, 'MobileNumber')}
                                            className='inputbox' placeholder='Enter Contact No' focused id="outlined-basic" label="Contact No" variant="outlined" name="MobileNumber" value={edituser.MobileNumber} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.MobileNumber ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.MobileNumber}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleblur("Address")}
                                            onFocus={(e) => handleerror(null, 'Address')}
                                            className='inputbox' placeholder='Enter Address' focused id="outlined-basic" label="Address" variant="outlined" name="Address" value={edituser.Address} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.Address ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.Address}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleblur("City")}
                                            onFocus={(e) => handleerror(null, "City")}
                                            className='inputbox' placeholder='Enter City' focused id="outlined-basic" label="City" variant="outlined" name="City" value={edituser.City} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.City ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.City}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='inputboxnamerow'>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleblur("State")}
                                            onFocus={(e) => handleerror(null, 'State')}
                                            className='inputbox' placeholder='Enter State' focused id="outlined-basic" label="State" variant="outlined" name="State" value={edituser.State} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.State ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.State}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                                <div className='input-wrap' >
                                    <div style={{ height: '80%', justifyContent: 'center', alignItems: 'center', display: 'flex', width: '100%' }}>
                                        <TextField onChange={editChange}
                                            onBlur={(e) => handleblur("Credits")}
                                            onFocus={(e) => handleerror(null, 'Credits')}
                                            className='inputbox' placeholder='Enter Credits' focused id="outlined-basic" label="Credits" variant="outlined" name="Credits" value={edituser.Credits} />
                                    </div>
                                    <div style={{ height: '20%', width: '100%', display: 'flex', justifyContent: 'center', }}>
                                        <div style={{ width: '85%', }}>

                                            {EditErrormessage.Credits ? (
                                                <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{EditErrormessage.Credits}</p>
                                            ) : null}


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ThemeProvider>
                    </div>
                    <div className='buttoncontainer1'>
                        <div className='buttongap1'></div>
                        <div className='buttonbox1'>
                            <button className='button1a' onClick={handleCloseedit}><p className='button1atext'>Cancel</p></button>
                            <button className='button2a' onClick={handleEdituser}><p className='button2atext'>Save</p></button>
                        </div>
                    </div>

                </div>


            </Dialog>
            {/* edit user end */}

            {/* delete */}
            <Dialog
                open={deleteact}
                onClose={handleClosedeleteact}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div style={{ width: 483, height: 175, backgroundColor: '#FFFFFF', }}>
                    <div className='password4'>
                        <div className='space2'></div>
                        <div className='text2'>
                            <p className='word1a'>Confirm Delete</p>
                        </div>
                    </div>
                    <div className='logoutcontainer'>
                        <div className='logoutcontainer1'>
                            <p className='logoutcontainer1text'>Are you sure, do you want to delete -{deleteuserdetail.UserName}?</p>
                        </div>

                    </div>

                    <div className='buttoncontainer2'>
                        <div className='buttongap1'></div>
                        <div className='buttonbox1'>
                            <button className='button1a' onClick={handleClosedeleteact}><p className='button1atext' >No</p></button>
                            <button className='button2a' onClick={confirmdeleteuser} ><p className='button2atext'>Yes</p></button>
                        </div>
                    </div>
                </div>
            </Dialog>
            {/* delete end */}
        </>
    )
}

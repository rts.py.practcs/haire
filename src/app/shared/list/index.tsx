import { CircularProgress, createMuiTheme, createTheme, Dialog, makeStyles, Pagination, styled, ThemeProvider } from '@mui/material'
import { useEffect, useState } from 'react'
import './list.scss'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { orange, red } from '@mui/material/colors';
import axios from 'axios';
import { resumesurl, resumeurl } from '../../core/service/configurls';

export const Listview = () => {

    const [page, setpage] = useState(1)
    const [tabledata, settabledata] = useState([])
    var [totalItems, setTotalItems] = useState<any>(0);
    const [pageCount, setPageCount] = useState(1)
    const [loading, setLoading] = useState(true);

    const loadpost = async () => {
        const res = await axios.get(`${resumeurl}?limit=10&offset=${totalItems}`)
        settabledata(res.data.results)
        let isFloat = Number((res.data.count / 10)) === (res.data.count / 10) && (res.data.count / 10) % 1 !== 0
        setPageCount(!isFloat ? (res.data.count / 10) : parseInt((res.data.count / 10).toString().split('.')[0]) + 1)
        setLoading(false);
    }

    useEffect(() => {
        loadpost()
    }, [])


    const outerTheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });

    const [open, setOpen] = useState(false)
    const [resumeview, setResumeview] = useState({ file: '' })

    const viewresume = async (id: any) => {
        setOpen(true);
        const response = await axios.get(`${resumesurl}/${id}`)
        setResumeview(response.data.data)
    };

    interface Item { id: number; }

    const deleteresume = (id: any) => {
        setOpen(false);
        axios.delete(`${resumeurl}/${id}`).then(() => {
            const del = tabledata.filter((item: Item) => id !== item.id)
            settabledata(del)
        })
    }

    const handleClose = () => {
        setOpen(false);
    };
    /////////pagination//////////
    const handleChangePage = (event: any, value: any) => {
        // setpage(page + 1)
        totalItems = (value * 10) - 10;
        setTotalItems(totalItems)
        loadpost()
        // setpage(value - 1);
        setpage(value);
        // console.log(value)
        // setTotalItems(totalItems + 10)
    };

    return (
        <>
            <div className='container1'>
                <div className='container2'>
                    {loading ? (
                        <CircularProgress />
                    ) :
                        <TableContainer className='container-table'     >
                            <Table sx={{ minWidth: 650, border: 'none' }} aria-label="simple table">
                                <TableHead style={{ backgroundColor: 'white' }}>
                                    <TableRow sx={{ borderBottom: '1.5px solid #1D2E44', border: 'none' }}>
                                        <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#008D96', lineHeight: '31px', backgroundColor: 'white' }}>Location</TableCell>
                                        <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#008D96', lineHeight: '31px', backgroundColor: 'white' }}>Experience</TableCell>
                                        <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#008D96', lineHeight: '31px', backgroundColor: 'white' }}>Company</TableCell>
                                        <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#008D96', lineHeight: '31px', backgroundColor: 'white' }}>Designation</TableCell>
                                        <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#008D96', lineHeight: '31px', backgroundColor: 'white' }}>Manage</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody >
                                    {
                                        tabledata.map(({ id, location, experience, company, designation }) => (
                                            <TableRow key={id} style={{ backgroundColor: 'white' }}>
                                                <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#989898', lineHeight: '31px' }} >{location}</TableCell>
                                                <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#989898', lineHeight: '31px' }}>{experience}</TableCell>
                                                <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#989898', lineHeight: '31px' }}>{company}</TableCell>
                                                <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#989898', lineHeight: '31px' }} >{designation}</TableCell>
                                                <TableCell sx={{ fontSize: '16px', fontWeight: '700', color: '#989898', lineHeight: '31px' }} ><a className='acion-btn' onClick={() => viewresume(id)}>View</a> <a className='acion-btn' onClick={() => deleteresume(id)}>Delete</a></TableCell>
                                            </TableRow>
                                        ))
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    }
                </div>
            </div>
            <div className='paginationcontainer'>
                <div className='paginationbox'>
                    <ThemeProvider theme={outerTheme}>
                        <Pagination page={page} count={pageCount} onChange={handleChangePage} color='primary'></Pagination>
                    </ThemeProvider>
                </div>
            </div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <div style={{ width: '38vw', height: '90vh', backgroundColor: '#F3F3F6', overflow: 'auto' }}>
                    <img style={{ height: '99.7%', width: '100%' }} src={resumeview.file} alt="resume" />
                </div>
            </Dialog>
        </>
    )
}
import { MenuItem, MenuList } from "@mui/material"
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import profileimg from '../../assets/images/profileimg.svg';
import { useNavigate } from "react-router-dom";
import '../home/home.scss'
import React, { useEffect, useState } from "react";
import { Profile } from "../viewprofile/profile";
import { admindetails, userurl } from "../../core/service/configurls";
import axios from "axios";

export const Sidemenu = () => {

    const [userdetail, setUserdetail] = useState({ UserName: '' })
    const id = localStorage.getItem("id");
    const UserName = localStorage.getItem("username")

    const [open1, setOpen1] = React.useState(true);

    const loadpost = async () => {
        const res = await axios.get(`${admindetails}/${id}`)
        setUserdetail(res.data)
        setOpen1(false)
    }

    const loaduser = async () => {
        const res = await axios.get(`${userurl}/${id}`)
        setUserdetail(res.data)
        setOpen1(true)
    }
    const localrole = localStorage.getItem("role")
    useEffect(() => {
        let role = localrole;

        if (role == "Admin") {
            loadpost()
        }
        if (role == "user") {
            loaduser()
        }
    }, [])

    const navigate = useNavigate();

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };

    return (
        <>
            {open ? (
                <>
                    <Profile />
                </>
            ) : (
                <>
                    <div className='side-menu-wrap'>
                        {open1 ?
                            <MenuList className="menu-list" >
                                <MenuItem className='menu-item' onClick={() => navigate("/home/dashboard")}>Dashboard</MenuItem>
                                <MenuItem className="menu-item" onClick={() => navigate("/home/resumes")}>Resumes</MenuItem>
                                <MenuItem className="menu-item" onClick={() => navigate("/home/accessment")}>Accessment</MenuItem>



                            </MenuList> :
                            <MenuList className="menu-list" >
                                <MenuItem className='menu-item' onClick={() => navigate("/home/dashboard")}>Dashboard</MenuItem>
                                <MenuItem className="menu-item" onClick={() => navigate("/home/users")}>Users</MenuItem>
                                <MenuItem className="menu-item" onClick={() => navigate("/home/resumes")}>Resumes</MenuItem>
                            </MenuList>
                        }
                    </div>
                    <div className="profile-box-wrap" onClick={handleOpen}>
                        <div className="profile-img-wrap">
                            <img className='profileimg' src={profileimg} alt='profileimg' />
                        </div>
                        <div className='profile-name-wrap' onClick={handleOpen}>
                            <p className='profile-name'>{userdetail.UserName}</p>
                            <ArrowForwardIosIcon className='profile-icon'></ArrowForwardIosIcon>
                        </div>
                    </div>
                </>
            )}
        </>
    )
}
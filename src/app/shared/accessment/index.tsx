import { Accordion, AccordionDetails, AccordionSummary, Card, CardActionArea, CardContent, CardMedia, createTheme, Dialog, Drawer, FormControlLabel, FormGroup, IconButton, InputAdornment, makeStyles, styled, TextField, ThemeProvider, Typography, withStyles } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { Input } from 'semantic-ui-react'
import down from '../../assets/images/down.svg'
import star from '../../assets/images/Star 1.svg'
import './accessment.scss'
import Switch, { SwitchProps } from '@mui/material/Switch';
import EyeIconButton from '../../assets/images/eyeheart.svg'
import deletebox from '../../assets/images/deletebox.svg'
// import Rightsidemenu from '../rightsidemenu/intex'
import axios from 'axios'

const IOSSwitch = styled((props: SwitchProps) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} />
))(({ theme }) => ({
    width: 62,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,

        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(32px)',
            color: 'teal',
            '& + .MuiSwitch-track': {
                backgroundColor: 'rgba(207, 228, 232, 1)',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: 'yellow',
            border: '6px solid orange',
        },

        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? 'rgba(207, 228, 232, 1)' : 'rgba(207, 228, 232, 1)',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },

}));
// const useStyles = makeStyles((theme: any) => ({
//     drawer: {

//     }
// }))
export const Accessment = () => {

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(open);
    };
    const [add, setadd] = React.useState(false);
    const handleClickadd = () => {
        setadd(true);
    };

    const [drawerOpen, setDrawerOpen] = useState(false);

    const toggleDrawer = () => {
        setDrawerOpen(true);
    };

    /////////////////////get method/////
    const [card, setcard] = useState<any[]>([])
    const assessget = async () => {
        const res = await axios.get('http://192.168.18.87:8000/api/assessment')
        console.log(res.data)
        setcard(res.data)
    }
    useEffect(() => {
        assessget()
    }, [])
    // /////////////////////////////////////////
    const assessdelete = async (id: any) => {
        const res = await axios.delete(`http://192.168.18.87:8000/api/assessment/${id}`)
        setcard(res.data)
    }
    const initialValues = { Name: '', Tags: '', Description: '', Question: '' }
    const [userData, setUserData] = useState({
        Name: '', Tags: '', Description: '', Question: ''
    })
    const { Name, Tags, Description, Question } = userData;
    const assessmentdetails = async (data: any) => {
        try {
            const response = await axios.post('http://192.168.18.87:8000/api/assessment', data)
            setcard(response.data)
        } catch (err) {
            console.log('err')
        }
    }
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setUserData({ ...userData, [name]: value });
    }
    const data = { Name, Tags, Description, Question }
    const save = () => {
        assessmentdetails(data)
        setDrawerOpen(false)
    }
    // const [open, setOpen] = React.useState(false);
    // const handleOpen = () => {
    //     setOpen(!open);
    // };
    const inputtheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });
    // /////////////////////////////////////////assesssment popup///////////////////////////
    const [assesspopup, setassesspopup] = useState(false)
    const handleClose = () => {
        setassesspopup(false);
    };
    const handleClickOpen = () => {
        setassesspopup(true);
    };
    return (
        <>
            <Drawer
                anchor={'right'}
                open={drawerOpen}
                onClose={toggleDrawer}
                className='drawercontainer'


            >
                <div className='fullbody'>
                    <div className='createcontainer'>
                        <div className='createtextcontainer'>
                            <p className='createtext'>Create Assessment</p>
                        </div>
                        <div className='buttoncontainer'>
                            <button className='canclebutton' onClick={() => setDrawerOpen(false)}>cancle</button>
                            <button className='savebutton' onClick={save}>save</button>
                        </div>

                    </div>
                    <div className='resumecontainer'>
                        <Accordion id='assessmentdetail'>
                            <AccordionSummary
                                // style={{ backgroundColor: 'red' }}
                                expandIcon={<img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <div id='assessmentdetailtext' >Assessment Details</div>
                            </AccordionSummary>
                            <AccordionDetails id='detailcontainer' >
                                <ThemeProvider theme={inputtheme}>
                                    <div id='emty'>
                                        <TextField value={Name}
                                            onChange={handleChange}
                                            name="Name"
                                            className='inputbox' placeholder='Enter Name' label='Assessment Name' ></TextField>
                                        <TextField value={Tags}
                                            onChange={handleChange}
                                            name="Tags"
                                            className='inputbox' placeholder='Enter Tags' label='Assessment Tag'></TextField>

                                    </div>
                                    <div id='emty1'>
                                        <TextField value={Description}
                                            className='inputbox1'
                                            onChange={handleChange}
                                            name="Description"
                                            placeholder='Enter Description'
                                            label='Assessment Description'></TextField>
                                    </div>
                                </ThemeProvider>

                            </AccordionDetails>


                        </Accordion>

                        <Accordion id='assessmentdetail'>
                            <AccordionSummary
                                // style={{ backgroundColor: 'red' }}
                                expandIcon={<img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <div id='assessmentdetailtext' >Question</div>
                            </AccordionSummary>
                            <AccordionDetails id='question' >
                                <TextField style={{ height: '100%', width: '100%', }}
                                    id="filled-textarea"
                                    // label="Multiline Placeholder"
                                    // placeholder="Placeholder"
                                    multiline
                                    variant='filled'
                                    value={Question}
                                    name='Question'
                                    onChange={handleChange}

                                />

                            </AccordionDetails>


                        </Accordion>
                    </div>
                </div>
            </Drawer>
            <Dialog
                open={assesspopup}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            ></Dialog>
            <div className="users-wrap">
                <div className="searchboxadd">
                    <div className='resumestextboxadd'>
                        <p className='resumestextadd'>Assessment</p>
                    </div>
                    <div className='searchtextboxadd'>
                        <div className="searchbox1add" >
                            <Input
                                disableUnderline
                                placeholder="SEARCH BY NAME"
                                id="input-with-icon-adornment"
                                className="searchadd1"
                                endAdornment={
                                    <InputAdornment position="end" onClick={handleOpen} style={{ height: '10px', width: '20px', }}>
                                        <img src={down} style={{ height: '10px', width: '20px', cursor: 'pointer' }}></img>
                                    </InputAdornment>
                                }
                            />
                        </div>
                        {open ? (
                            <div className="menu">
                                <input className="insidedropdown" placeholder="SEARCH BY COMPANY">
                                </input>
                                <input className="insidedropdown" placeholder="SEARCH BY EMAIL">
                                </input>
                            </div>
                        ) : null}
                    </div>
                    <div className='buttonboxadd'>
                        <button className='buttonadd' onClick={() => toggleDrawer()}>
                            <p className='buttontextadd'>Create</p>
                        </button>
                    </div>
                </div>
                <div className='cardcontainer'>
                    {card.map(({ id, Name, Tags, Description }) => (<div
                        key={id}
                        className='cardbox'>


                        <Card className='card'>
                            <div className='contentcontainer'>
                                <div className='textbox'>
                                    <div className='assessmentcontainer'><p className='assesssmenttext'>{Name}</p></div>
                                    <div className='frondendcontainer'>
                                        <img src={star} className='frondendimg'></img>
                                        <p className='frondendtest'>{Tags}</p>
                                    </div>
                                    <div className='frondendcontainer'>
                                        <img src={star} className='frondendimg'></img>
                                        <p className='frondendtest'>{Description}</p>
                                    </div>
                                    {/* <div className='frondendcontainer'>
                                        <img src={star} className='frondendimg'></img>
                                        <p className='frondendtest'>Node</p>
                                    </div> */}
                                </div>
                            </div>
                            <div className='iconcontainer'>



                                <IOSSwitch sx={{ m: 1 }} defaultChecked />

                                {/* <EyeIconButton ></EyeIconButton> */}
                                <InputAdornment position='start' style={{ height: '50%', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', }} >
                                    <button onClick={handleClickOpen} style={{ border: 'none', backgroundColor: 'transparent' }}>
                                        <img src={EyeIconButton}></img>
                                    </button>

                                </InputAdornment>
                                <InputAdornment position='start' onClick={() => assessdelete(id)} style={{ height: '150%', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', }} >
                                    <img src={deletebox}></img>
                                </InputAdornment>
                            </div>
                        </Card>
                    </div>
                    ))}



                </div>
            </div>
        </>
    )
}

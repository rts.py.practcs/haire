import './header1.scss'
import logoimg from '../../assets/images/logo.svg'
import MenuIcon from '@mui/icons-material/Menu';
import React from 'react';
import { Button, Drawer } from '@mui/material';
import { Sidemenu } from '../sidemenu';

type Anchor = 'left';

export const Header1 = () => {
    const currentPath = window.location.pathname

    const [state, setState] = React.useState({
        left: false,
    });
    const toggleDrawer =
        (anchor: Anchor, open: boolean) =>
            (event: React.KeyboardEvent | React.MouseEvent) => {
                if (
                    event.type === 'keydown' &&
                    ((event as React.KeyboardEvent).key === 'Tab' ||
                        (event as React.KeyboardEvent).key === 'Shift')
                ) {
                    return;
                }

                setState({ ...state, [anchor]: open });
            };
    return (
        <>
            <div id="header">
                <div className='logo-wrap' >
                    <img className='logo-img' alt='logo' src={logoimg} />
                    <h2 className='logo-line'>RECRUITMENT PORTAL</h2>
                    {(['left'] as const).map((anchor) => (
                        <React.Fragment key={anchor}>
                            <MenuIcon onClick={toggleDrawer(anchor, true)} className='menu-btn' />
                            <Drawer
                                anchor={anchor}
                                open={state[anchor]}
                                onClose={toggleDrawer(anchor, false)}
                            >
                                <div className='side-drawer'>
                                    <Sidemenu />
                                </div>
                            </Drawer>
                        </React.Fragment>
                    ))}
                </div>
            </div>
        </>
    )
}
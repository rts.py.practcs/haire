import './profile.scss'
import React, { useEffect, useState } from 'react';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';


import { useNavigate } from 'react-router-dom';
import leftarrowpic from '../../assets/images/Vector.svg'
import profileimg from '../../assets/images/profileimg.svg'
import { createTheme, Dialog, TextField, ThemeProvider } from '@mui/material';
import { Sidemenu } from '../sidemenu';
import axios from 'axios';
import { admindetails, userurl } from '../../core/service/configurls';

export const Profile = () => {

  const [userdetail, setUserdetail] = useState({ UserName: '', Email: '', MobileNumber: '', Address: '', City: '' })
  const id = localStorage.getItem("id");

  const loadpost = async () => {
    const res = await axios.get(`${admindetails}/${id}`)
    setUserdetail(res.data)
  }

  const loaduser = async () => {
    const res = await axios.get(`${userurl}/${id}`)
    setUserdetail(res.data)
  }

  useEffect(() => {
    let role = localStorage.getItem("role")
    if (role = "Admin") {
      loadpost()
    } if (role = "user") {
      loaduser()
    }
  }, [])

  // vi changepassword popup
  const [change, setchange] = React.useState(false)

  const handleClickOpenchange = () => {
    setchange(true);
  };
  const handleClosechange = () => {
    setchange(false);
  };

  const [errorMessage2, setErrorMessage2] = React.useState<any>({});
  const [changepassword, setchangepassword] = React.useState({
    oldpassword: '',
    newpassword: '',
    confirmpassword: '',
  })

  // const emailpatten = RegExp(/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/)
  const passreg: any = RegExp(/^[A-Z0-9]{6}$/)
  const changepassrex: any = RegExp(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)


  const handleErr2 = (err: any, pops: any) => {
    setErrorMessage2((prev: any) => ({ ...prev, [pops]: err }))
  }
  const handleChanges2 = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, text: string) => {
    setchangepassword((prev: any) => ({ ...prev, [text]: e.target.value }))
  }

  ////////////////////////////Edit Profie//////////////////////////////////////////

  const nameformat = RegExp(/^[A-Z]{1}[a-z]/)
  const mobilnumber = RegExp(/^[6-9]\d{9}$/gi)
  const emailpatten = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)

  const [editerrror, setediterror] = React.useState<any>('')
  const handlEditChange = (e: any) => {
    const { name, value } = e.target;
    setUserdetail({ ...userdetail, [name]: value })
  }
  const handleEditerror = (err: any, pops: any) => {
    setediterror((prev: any) => ({ ...prev, [pops]: err }))
  }
  const handleEditBlur = (value: any) => {
    if (value == 'UserName') {
      if (!userdetail.UserName) {
        handleEditerror('Enter your first Name', 'UserName')
      } else if (!userdetail.UserName.match(nameformat)) {
        handleEditerror('Enter Your Name First Letter Capitial ', 'UserName')
      }
    }
    if (value == 'City') {
      if (!userdetail.City) {
        handleEditerror('Enter your City', 'City')
      }
    }
    if (value == 'Email') {
      if (!userdetail.Email) {
        handleEditerror('Enter your Email ID ', 'Email')
      } else if (!userdetail.Email.match(emailpatten)) {
        handleEditerror('Enter Valid Email ID ', 'Email')
      }
    }
    if (value == 'MobileNumber') {
      if (!userdetail.MobileNumber) {
        handleEditerror('Enter your MobileNumbere', 'MobileNumber')
      } else if (!userdetail.MobileNumber.match(mobilnumber)) {
        handleEditerror('Enter Your Valid MobileNumber ', 'MobileNumber')
      }
    }
    if (value == 'Address') {
      if (!userdetail.Address) {
        handleEditerror('Enter your Address', 'Address')
      }
    }

  }
  // /////////////////////////////////////////////////////////////////////////////// 
  const handleBlur2 = (value: any) => {
    if (value == "oldpassword") {
      if (!changepassword.oldpassword) {
        handleErr2("Enter Your Password ", "oldpassword")
      } else if (changepassword.confirmpassword !== changepassword.newpassword) {
        handleErr2("Enter The Valid Password ", "oldpassword")
      }
    }
    if (value == "newpassword") {
      if (!changepassword.newpassword) {
        handleErr2("Enter Your New Password", "newpassword")
      } else if (!changepassword.newpassword.match(changepassrex)) {
        handleErr2("Password Doesn't Match ", "newpassword")
      }
    }
    if (value == "confirmpassword") {
      if (!changepassword.confirmpassword) {
        handleErr2("Enter Your Confirm Password ", "confirmpassword")
      } else if (changepassword.confirmpassword !== changepassword.newpassword) {
        handleErr2("Password Doesn't Match ", "confirmpassword")
      }
    }
  }
  /////////////////////////////////////////////change password///////////////////////
  // const passwordchange=async ()=>{
  //   const change=await axios.put(`http://192.168.18.87:8000/api/password${id}`),{
  //     setchangepassword(),

  //   }
  // }


  // edit profile
  const [edit, setEdit] = React.useState(false)

  const [edituser, setEdituser] = useState(
    { UserName: '', Email: '', MobileNumber: '', Address: '', LastName: '' }
  )

  const handleClickedit = async (id: any) => {
    setEdit(true);
    // const response = await axios.get(`${admindetails}/${id}`)
    // setEdituser(response.data)
  };
  const handleCloseedit = () => {
    setEdit(false);
  };

  // delete account
  const [deleteact, setDeleteact] = React.useState(false)
  const handleClickdelete = () => {
    setDeleteact(true);
  }
  const handleClosedeleteact = () => {
    setDeleteact(false);
  }
  ///logout/////
  const [logoutact, setlogoutact] = React.useState(false)
  const handleClicklogout = () => {
    setlogoutact(true);
  }
  const handleCloselogoutact = () => {
    setlogoutact(false);
  }

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(!open);
  };

  const navigate = useNavigate()

  // edit details
  async function handleEditadmin(evt: any) {
    evt.preventDefault()
    const { UserName, Email, MobileNumber, Address, City } = userdetail
    const editData = { UserName, Email, MobileNumber, Address, City }
    axios.put(`${admindetails}/update/${id}`, editData)

    setEdit(false);
  }
  // Change passeod
  async function handlechangepassword(evt: any) {
    evt.preventDefault()
    const { oldpassword, newpassword, confirmpassword } = changepassword
    const newpasswordchange = { oldpassword, newpassword, confirmpassword }
    axios.put(`http://192.168.18.87:8000/api/passwords`, newpasswordchange)

    setchange(false);
  }


  // themes
  const inputtheme = createTheme({
    palette: {
      primary: {
        main: '#008D96',
      }
    },
  });

  // handleDeleteaccount

  async function handleDeleteaccount() {
    axios.delete(`${admindetails}/update/${id}`)
    navigate('/')
  }
  return (
    <>
      {open ? (
        <>
          <Sidemenu />
        </>
      ) : (
        <>
          <div className='sidebar'>
            <center>
              <img src={profileimg} className='fluid contact-img dotted-line' />
              <h3 className="Profilename">{userdetail.UserName}</h3>
              <p className="mailid" > {userdetail.Email}  </p>
              <div className='backbtn-wrap'>
                <img src={leftarrowpic} className="leftarrowpic" alt='leftarrow' onClick={handleOpen} />
                <p className='settingline'>Settings</p>
              </div>

              <div onClick={() => handleClickedit(id)} className="dotted-line1">
                <SettingsOutlinedIcon className='edit' />&nbsp;
                Edit Profile
              </div>

              <div onClick={handleClickOpenchange} className="dotted-line1">
                <SettingsOutlinedIcon className='edit' />&nbsp;
                Change Password
              </div>

              <div onClick={handleClickdelete} className="dotted-line1">
                <  SettingsOutlinedIcon className='edit' /> &nbsp;
                Delete Account
              </div>

              <div onClick={handleClicklogout} className="dotted-line1">
                <  SettingsOutlinedIcon className='edit' /> &nbsp;
                Logout
              </div>
            </center>
          </div>
        </>
      )}

      {/* edit profile */}
      <Dialog
        open={edit}
        onClose={handleCloseedit}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div style={{ width: 600, height: 360, backgroundColor: 'white' }}>
          <div className='password3'>
            <div className='space1'></div>
            <div className='text1'>
              <p className='word1a'>Edit Profile</p>
            </div>
          </div>
          <div className='inputwrap'>
            <ThemeProvider theme={inputtheme}>
              <div className="inputsplit">
                <TextField
                  onChange={handlEditChange}
                  onBlur={(e) => handleEditBlur('UserName')}
                  onFocus={(e) => handleEditerror(null, "UserName")}
                  className='inputbox1' placeholder='Enter First Name' focused id="outlined-basic"
                  label="First Name" variant="outlined"
                  name="UserName"
                  value={userdetail.UserName}
                />
                <TextField
                  onChange={handlEditChange}
                  onBlur={(e) => handleEditBlur('Email')}
                  onFocus={(e) => handleEditerror(null, "Email")}
                  className='inputbox1' placeholder='Enter your Email ID' focused id="outlined-basic"
                  label="Email" variant="outlined"
                  name="Email"
                  value={userdetail.Email}
                />

              </div>
              <div style={{ height: '5%', width: '100%', display: 'flex', flexDirection: 'row' }}>
                <div style={{ width: '50%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <div style={{ width: '80%', height: '100%', fontSize: 12 }}>{editerrror.UserName ? (
                    <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{editerrror.UserName}</p>)
                    : null}</div>
                </div>
                <div style={{ width: '50%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <div style={{ width: '80%', height: '100%', fontSize: 12 }}>{editerrror.Email ? (
                    <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{editerrror.Email}</p>)
                    : null}</div>
                </div>
              </div>
              <div className="inputsplit">

                <TextField
                  onChange={handlEditChange}
                  onBlur={(e) => handleEditBlur('MobileNumber')}
                  onFocus={(e) => handleEditerror(null, "MobileNumber")}
                  className='inputbox1' placeholder='Enter Contact Number' focused id="outlined-basic"
                  label="Contact Number" variant="outlined"
                  name="MobileNumber"
                  value={userdetail.MobileNumber}
                />
                <TextField
                  onChange={handlEditChange}
                  onBlur={(e) => handleEditBlur('City')}
                  onFocus={(e) => handleEditerror(null, "City")}
                  className='inputbox1' placeholder='Enter City' focused id="outlined-basic"
                  label="City" variant="outlined"
                  name="City"
                  value={userdetail.City}
                />
              </div>
              <div style={{ height: '19%', width: '100%', display: 'flex', flexDirection: 'row' }}>
                <div style={{ width: '50%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <div style={{ width: '80%', height: '100%', fontSize: 12 }}>{editerrror.MobileNumber ? (
                    <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{editerrror.MobileNumber}</p>)
                    : null}</div>
                </div>
                <div style={{ width: '50%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <div style={{ width: '80%', height: '100%', fontSize: 12 }}>{editerrror.City ? (
                    <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{editerrror.City}</p>)
                    : null}</div>
                </div>
              </div>
              <div style={{ height: '40%', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <TextField
                  onChange={handlEditChange}
                  onBlur={(e) => handleEditBlur('Address')}
                  onFocus={(e) => handleEditerror(null, "Address")}
                  className='inputbox2' placeholder='Enter Your Address' focused id="outlined-basic"
                  label="Address" variant="outlined"
                  name="Address"
                  value={userdetail.Address}
                />
              </div>
              <div style={{ height: '7%', width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <div style={{ width: '80%', height: '100%', display: 'flex', fontSize: 12 }}>
                  {editerrror.Address ? (
                    <p style={{ color: "red", fontSize: 10, fontWeight: '400', paddingLeft: 15, }}>{editerrror.Address}</p>)
                    : null}
                </div>
              </div>
            </ThemeProvider>
          </div>
          <div className='buttoncontainer1'>
            <div className='buttongap1'></div>
            <div className='buttonbox1'>
              <button className='button1a' onClick={handleCloseedit}><p className='button1atext' >Cancel</p></button>
              <button className='button2a' onClick={handleEditadmin} ><p className='button2atext'>Change</p></button>
            </div>
          </div>
        </div>
      </Dialog>

      {/* change password */}
      <Dialog
        open={change}
        onClose={handleClosechange}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div style={{ width: 402, height: 360, backgroundColor: 'white' }}>
          <div className='password3'>
            <div className='space1'></div>
            <div className='text1'>
              <p className='word1a'>Change Password</p>
            </div>
          </div>

          <div className='inputcontainer2a'>
            <ThemeProvider theme={inputtheme}>
              <div style={{ height: '50%' }}>
                <div style={{ height: '20%', }}></div>
                <div style={{ height: '55%', justifyContent: 'center', alignItems: 'center', display: 'flex', }}>
                  <TextField className='inputbox'
                    focused id="outlined-basic"
                    label="Current Password"
                    variant="outlined"
                    name="CurrentPassword"
                    placeholder='Enter Your Current Password'
                    onFocus={(e) => handleErr2(null, 'oldpassword')}
                    onChange={e => handleChanges2(e, 'oldpassword')}
                    value={changepassword.oldpassword}
                    onBlur={(e) => {
                      handleBlur2('oldpassword')
                    }} ></TextField>
                </div>
                <div style={{ height: '20%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                  <div style={{ width: '85%', height: '100%' }}>
                    {errorMessage2.oldpassword ? (
                      <p style={{ color: "red", margin: 7, fontSize: 10, fontWeight: '400', paddingLeft: 13, }}>{errorMessage2.oldpassword}</p>
                    ) : null}
                  </div>
                </div>
              </div>
              <div style={{ height: '50%' }}>
                <div style={{ height: '20%', }}></div>
                <div style={{ height: '55%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                  <TextField className='inputbox'
                    focused id="outlined-basic"
                    label="New Password"
                    variant="outlined"
                    name="NewPassword"
                    placeholder='Enter Your New Password'
                    onFocus={(e) => handleErr2(null, 'newpassword')}
                    onChange={e => handleChanges2(e, 'newpassword')}
                    value={changepassword.newpassword}
                    onBlur={(e) => {
                      handleBlur2('newpassword')
                    }} />
                </div>
                <div style={{ height: '20%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                  <div style={{ width: '85%', height: '100%', }}>
                    {errorMessage2.newpassword ? (
                      <p style={{ color: "red", margin: 7, fontSize: 10, fontWeight: '400', paddingLeft: 13, }}>{errorMessage2.newpassword}</p>
                    ) : null}
                  </div>
                </div>
              </div>
              <div style={{ height: '50%' }}>
                <div style={{ height: '20%', }}></div>
                <div style={{ height: '55%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                  <TextField className='inputbox'
                    placeholder='Confirm Your New Password'
                    focused id="outlined-basic"
                    label="Confirm Password"
                    variant="outlined"
                    name="ConfirmPassword"
                    onFocus={(e) => handleErr2(null, 'confirmpassword')}
                    onChange={e => handleChanges2(e, 'confirmpassword')} value={changepassword.confirmpassword} onBlur={(e) => {
                      handleBlur2('confirmpassword')
                    }} />
                </div>
                <div style={{ height: '20%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                  <div style={{ width: '85%', height: '100%', }}>
                    {errorMessage2.confirmpassword ? (
                      <p style={{ color: "red", margin: 7, fontSize: 10, fontWeight: '400', paddingLeft: 13, }}>{errorMessage2.confirmpassword}</p>
                    ) : null}
                  </div>
                </div>
              </div>
            </ThemeProvider>
          </div>
          <div className='buttoncontainer1'>
            <div className='buttongap1'></div>
            <div className='buttonbox1'>
              <button className='button1a' onClick={handleClosechange}><p className='button1atext' >Cancel</p></button>
              <button onClick={handlechangepassword} className='button2a' ><p className='button2atext' >Change</p></button>
            </div>
          </div>
        </div>

      </Dialog>

      {/* delete */}
      <Dialog
        open={deleteact}
        onClose={handleClosedeleteact}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div style={{ width: 483, height: 175, backgroundColor: '#FFFFFF', }}>
          <div className='password4'>
            <div className='space2'></div>
            <div className='text2'>
              <p className='word1a'>Confirm Delete Account</p>
            </div>
          </div>
          <div className='logoutcontainer'>
            <div className='logoutcontainer1'>
              <p className='logoutcontainer1text'>Are you sure, do you want to delete this account?</p>
            </div>

          </div>

          <div className='buttoncontainer2'>
            <div className='buttongap1'></div>
            <div className='buttonbox1'>
              <button className='button1a' onClick={handleClosedeleteact}><p className='button1atext' >No</p></button>
              <button className='button2a' onClick={handleDeleteaccount} ><p className='button2atext'>Yes</p></button>
            </div>
          </div>
        </div>
      </Dialog>

      {/* logout */}
      <Dialog
        open={logoutact}
        onClose={handleCloselogoutact}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div style={{ width: 483, height: 175, backgroundColor: '#FFFFFF', }}>
          <div className='password4'>
            <div className='space2'></div>
            <div className='text2'>
              <p className='word1a'>Confirm Logout</p>
            </div>
          </div>
          <div className='logoutcontainer'>
            <div className='logoutcontainer1'>
              <p className='logoutcontainer1text'>Are you sure, do you want to logout?</p>
            </div>

          </div>

          <div className='buttoncontainer2'>
            <div className='buttongap1'></div>
            <div className='buttonbox1'>
              <button className='button1a' onClick={handleCloselogoutact}><p className='button1atext' >No</p></button>
              <button className='button2a' onClick={() => navigate('/')}><p className='button2atext'>Yes</p></button>
            </div>
          </div>
        </div>
      </Dialog>
    </>
  );
}



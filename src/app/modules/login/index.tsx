import './login.scss';
import stepsimg from '../../assets/images/stepsimg.svg'

import Visibility from '@mui/icons-material/Visibility';
import { Button, Container, FormControl, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, Input, TextField, Dialog, Typography, createTheme, ThemeProvider } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import { Loginservice } from '../../core/service/authservice';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Footer } from '../../shared/footer';
import { Header } from '../../shared/header';

import Icon from '../../assets/images/Icon.svg';
import arrow from '../../assets/images/Arrow Chevron Right.svg';

import { Formik } from "formik";
import * as Yup from "yup";
import axios from 'axios';
import { reseturl } from '../../core/service/configurls';
import { Userservice } from '../../core/service/users';

interface State {
    password: string;
    showPassword: boolean;
}

export const Login = () => {

    //popup  
    const [open, setOpen] = React.useState(false);
    const [open1, setOpen1] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);
    const [open3, setOpen3] = React.useState(false);


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    async function handleClickOpen1() {
        setOpen(false);
        setOpen1(true);
    };

    const handleClose1 = () => {
        setOpen1(false);
    };
    const handleClickOpen2 = () => {
        setOpen1(false);
        setOpen2(true);
    };

    const handleClose2 = () => {
        setOpen2(false);
    };
    const handleClickOpen3 = () => {
        setOpen2(false);
        setOpen3(true);
    };

    const handleClose3 = () => {
        setOpen3(false);
    };

    const [values, setValues] = React.useState<State>({
        password: '',
        showPassword: false,
    });

    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const navigate = useNavigate();

    const [otp, setotp] = useState<any>(new Array(4).fill(''))
    const handlechangeotp: any = (element: any, index: any) => {
        if (isNaN(element.value)) return false
        setotp([...otp.map((d: any, idx: any) => (idx === index) ? element.value : d)]);

        if (element.nextSibling) {
            element.nextSibling.focus()
        }
    }

    const { loginServiceSubmit } = Loginservice();

    const intialValues = { Email: '', Password: '' }
    const [loginData, setLoginData] = useState(intialValues)
    const { Email, Password } = loginData;

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setLoginData({ ...loginData, [name]: value });
    }

    const data = { Email, Password }
    const handleSubmit = (event: any) => {
        event.preventDefault()
        loginServiceSubmit(data)
    }
    //////////////login Email////////////////
    const emailpatten = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)
    const twopassword: any = RegExp(/^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{2}$||^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])[a-zA-Z0-9!@#\$%\^&\*]{8,}$/)
    const [errorMessage, setErrorMessage] = React.useState<any>({});
    const handleerror = (err: any, pops: any) => {
        setErrorMessage((prev: any) => ({ ...prev, [pops]: err }))
    }

    const handleBlur = (Value: any) => {
        if (Value == 'Email') {
            if (!loginData.Email) {
                handleerror("Enter Your Email", 'Email')
            } else if (!loginData.Email.match(emailpatten)) {
                handleerror("Enter The Valid Email", 'Email')
            }
        }
        if (Value == 'Password') {
            if (!loginData.Password) {
                handleerror("Enter Your Password", 'Password')
            } else if (!loginData.Password.match(twopassword)) {
                handleerror("Enter The Valid Password", 'Password')
            }
        }
    }

    // forget password
    const [postemail, setpostemail] = React.useState<any>('')
    const { handlechangepost, handleSubmitpostmethod } = Userservice()
    const inputtheme = createTheme({
        palette: {
            primary: {
                main: '#008D96',
            }
        },
    });
    return (
        <>
            <Header />
            <Container id='login-container' component='form' noValidate autoComplete='off'>
                <Grid container id='login-wrap'>
                    <ThemeProvider theme={inputtheme}>
                        <Grid item xs={12} sm={6} className='login-card-wrap'>
                            <div className='login-card'>
                                <p className='login-line'>Sign in</p>
                                <p className='login-sub-line'>enter to continue and explore within your grasp.</p>
                                <TextField
                                    variant='standard'
                                    size='small'
                                    label='Email Address'
                                    placeholder='Enter your Email Address'
                                    fullWidth
                                    name="Email"
                                    className='useridfield'
                                    value={loginData.Email}
                                    onChange={handleChange}
                                    onBlur={(e) => handleBlur('Email')}
                                    onFocus={(e) => handleerror(null, 'Email')}
                                />
                                <Typography>
                                    {
                                        errorMessage.Email ? (
                                            <p style={{ color: "red", margin: 7, fontSize: 10, fontWeight: '400', }}>{errorMessage.Email}</p>
                                        ) : null
                                    }
                                </Typography>
                                <FormControl variant='standard' fullWidth sx={{ marginTop: '20px' }}>
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <Input
                                        id="outlined-adornment-password"
                                        name="Password"
                                        placeholder='Enter your password'
                                        type={values.showPassword ? 'text' : 'password'}
                                        value={loginData.Password}
                                        onChange={handleChange}
                                        onBlur={(e) => handleBlur('Password')}
                                        onFocus={(e) => handleerror(null, 'Password')}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                    style={{ color: '#008D96' }}
                                                >
                                                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                                <Typography>
                                    {
                                        errorMessage.Password ? (
                                            <p style={{ color: "red", margin: 7, fontSize: 10, fontWeight: '400', }}>{errorMessage.Password}</p>
                                        ) : null
                                    }
                                </Typography>
                                <p className='login-forgot' onClick={handleClickOpen}>Forgot Password?</p>
                                <Button
                                    variant='contained'
                                    fullWidth
                                    className='login-btn'
                                    type="submit"
                                    onClick={handleSubmit}
                                >
                                    Login to Continue
                                </Button>
                            </div>
                        </Grid>
                    </ThemeProvider>

                    < Grid item xs={12} sm={6} className='steps-img-wrap' >
                        <div className='img-card'>
                            <h2 className='img-line'>Lorem Ipsum text to change content </h2>
                            <img className='stepsimg' src={stepsimg} alt='stepsimg' />
                        </div>
                    </Grid>
                </Grid>
            </Container >
            <Footer />
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >

                <div className='forgotpassworddialog'>
                    <div className='password'>
                        <div className='space'></div>
                        <div className='text'>
                            <p className='word1'>Reset Password</p>
                        </div>
                    </div>
                    <div className='inputcontainer'>
                        <input className='inputbox' placeholder='Phone, email or username' value={postemail} onChange={handlechangepost} ></input>
                    </div>
                    <div className='buttoncontainer'>
                        <div className='buttongap'></div>
                        <div className='buttonbox'>
                            <button className='button1' onClick={handleClose}><p className='button1text'>Cancel</p></button>
                            <button className='button2' onClick={handleSubmitpostmethod}><p className='button2text'>Reset</p></button>
                        </div>
                    </div>
                </div>

            </Dialog>
            {/* otp dialog */}
            <Dialog
                open={open1}
                onClose={handleClose1}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <div className='otppindialog'>
                    <div className='password1'>
                        <div className='text'>
                            <p className='word1'>Reset Password</p>
                        </div>

                    </div>
                    <div className='verifycodebox'>
                        <p className='verifycodeboxtext'>Enter Verification Code</p>
                    </div>
                    <div className='otpcodebox'>
                        <div className='otpcodesmallbox'>
                            <div className='smallbox1'>
                                <p className='smallbox1text'> Enter OTP Code That We Sent To</p>
                            </div>
                            <div className='smallbox2'>
                                <p className='smallbox2text'>Your Email ID</p>
                            </div>
                        </div>
                    </div>
                    {/* otpbox */}
                    <div className='otpcontainer'>
                        <div className='otpbox'>
                            {
                                otp.map((data: any, index: any) => {
                                    return <input className='smallbox3' maxLength={1} key={index} value={data} onChange={e => handlechangeotp(e.target, index)} onFocus={e => e.target.select()}></input>
                                })
                            }
                        </div>
                    </div>
                    {/* resend */}
                    <div className='resendcontainer'>
                        <div className='resendbox'>
                            <p className='resendtext'>Resend the Code</p>
                        </div>
                    </div>
                    {/* gggg */}
                    <div className='buttoncontainer'>
                        <div className='smallbox4'>
                            <div className='circle1'>
                                <button className='circle2' onClick={handleClickOpen2}>
                                    <img src={arrow}></img>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Dialog>
            {/* new password confirm password field */}
            <Dialog
                open={open2}
                onClose={handleClose2}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div className='confirmpassorddialog'>
                    <div className='password2'>
                        <div className='space'></div>
                        <div className='text'>
                            <p className='word1'>Reset Password</p>
                        </div>

                    </div>

                    <div className='inputcontainer2'>
                        <input className='inputbox2' placeholder='Enter Your New Password' ></input>
                        <input className='inputbox2' placeholder='Confirm Your New Password' ></input>
                    </div>
                    <div className='buttoncontainer1'>
                        <div className='buttongap'></div>
                        <div className='buttonbox1'>
                            <button className='button1'><p className='button1text'>Cancel</p></button>
                            <button className='button2' onClick={handleClickOpen3} ><p className='button2text'>Reset</p></button>
                        </div>
                    </div>
                </div>

            </Dialog>
            <Dialog
                open={open3}
                onClose={handleClose3}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div className='successfullpassword'>
                    <div className='password2'>
                        <div className='space'></div>
                        <div className='text'>
                            <p className='word1'>Sucessfully Reset Password</p>
                        </div>

                    </div>
                    <div className='rightlog'></div>
                    <div className='imagecontainer'>
                        <img src={Icon}></img>
                    </div>
                    <div className='sucess'>
                        <div className='sucess1'>
                            <div className='sucessbox'>
                                <p className='sucesstext'>You Can Login With Your</p>
                            </div>
                            <div className='sucessbox1'>
                                <p className='sucesstext'>New Password</p>
                            </div>
                        </div>
                    </div>
                    <div style={{ height: '3%' }}></div>
                    <div className='logincontainer'>
                        <button className='loginbutton' onClick={handleClose3}>
                            <p className='logintext'>Login</p>
                        </button>
                    </div>
                </div>
            </Dialog>
        </>
    );
}

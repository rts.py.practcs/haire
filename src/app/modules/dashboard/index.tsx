import { Container, Grid } from "@mui/material"
import { Header1 } from "../../shared/header1"
import Chart from "../../shared/charts/index"
import Charts from "../../shared/charts/dougnutchart"
import './dashboard.scss'

export const Dashboard = () => {
    return (
        <>
            <Header1 />
            <Container style={{ maxWidth: '100%', padding: '0px', height: '100vh', backgroundColor: 'lightgray', overflowY: 'auto' }}>
                <Grid container style={{ height: '60%', width: '95%', margin: '2.5%', borderRadius: '10px', backgroundColor: 'white' }}>
                    <Grid xs={6} style={{ alignItems: 'center', display: 'flex', paddingLeft: '1%' }}>
                        <Chart />
                    </Grid>
                    <Grid xs={6} style={{ alignItems: 'center', display: 'flex', paddingLeft: '1%' }}>
                        <Charts />
                    </Grid>
                </Grid>
                <Grid container columns={12} style={{ height: '23%', width: '100%', marginTop: '2%', marginBottom: '2%', flexDirection: 'row', display: 'flex', justifyContent: 'space-evenly' }}>
                    <Grid sx={{ borderRadius: '10px', height: '100%', width: '22%', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', display: 'flex' }}>
                        <h3> Users </h3 ><br />
                    </Grid >
                    <Grid sx={{ borderRadius: '10px', height: '100%', width: '22%', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', display: 'flex' }}>
                        <h3>Resumes  </h3 >
                    </Grid>

                </Grid>

            </Container>
        </>
    )
}
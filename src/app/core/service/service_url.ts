export const serviceUrl = {
  // Google
  report_context:
    "AKfycbwM7YYreJ86aeGhaNQ398ta7_5jNqKbF0Hw5WKv_v-cUqPIFweH9-_UHlml3TuOhugs/exec",
  update_file:
    "AKfycbyZWd-Su2PyqT3LkFsAxB_JMpq7Yulk0PEwx49X3lTWO6KnpoWcj5qRhnJvqpL_btZ8PA/exec",
  receive_score:
    "AKfycbxlrxR6v1RD9TGYuE407znmwkNTI2jxTz-sOz-vc_xOHu3Pj5_KYu4J_t2EQ4YkUJM/exec",
  add_context:
    "AKfycbzej9Dmf1V-MLq0Tvjv14AilFEPcjbtRrw0GJiLxVOy0dC9V61D2zhYdk_ClrigjBGztw/exec",

  // Others
  files_operation: "files",
  delete_file: "files/:fileId",
  update_sheet: "getupdatedsheet",
  get_answer: "answers",
  search_text: "search",
  get_image_link: "getimagelink",
  pdf_extract: "pdfextract"
};

import axios from 'axios';
import { useNavigate } from 'react-router';
import { loginurl } from '../configurls';
import { setAuthToken } from './authservice';

export const Loginservice = () => {
    const navigate = useNavigate()

    const loginServiceSubmit = (data: any) => {
        axios.post((`${loginurl}`), data).then(
            response => {
                if (response.data.status == "success") {
                    const token = response.data.access_token;
                    localStorage.setItem("token", token);
                    setAuthToken(token);

                    const id = response.data.user.id;
                    localStorage.setItem("id", id)

                    const role = response.data.user.Role;
                    localStorage.setItem("role", role)



                    navigate('/home')

                }
                else {
                    navigate('/')
                }
            }
        )
            .catch(error => {
                console.log(error);
            });
    }

    return { loginServiceSubmit }
}
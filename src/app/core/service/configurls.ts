// this is base url for all
export const baseurl = "http://192.168.18.87:8000/api"

// extent the baseurl with your needed module url from api document
export const loginurl = `${baseurl}/login`
export const userurl = `${baseurl}/recruiter`
export const resumeurl = `${baseurl}/file`
export const resumesurl = `${baseurl}/files`

export const admindetails = `${baseurl}/admin`

export const reseturl = `${baseurl}/reset`

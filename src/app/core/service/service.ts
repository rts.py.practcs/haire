import axios from "axios";
import { serviceUrl } from "./service_url";

export const serviceConfig = {
  // getHeader: function (is_json: any) {
  //   // let botType = localStorage.getItem("botType");
  //   let header:any = {
  //     headers:{
  //       Authorization:
  //         "Bearer " + process.env.REACT_APP_API_KEY,
  //     },
  //   };
  //   is_json && (header.headers["Content-Type"] = "application/json");
  //   return header;
  // },

  // buildUrl: function (url: any, url_type: any, query_params: any, url_params: any) {
  //   var return_url =
  //     process.env["REACT_APP_BASE_URLS_" + url_type] + serviceUrl[url];
  //   if (query_params) {
  //     return_url += "?";
  //     let loop_start = false;
  //     for (var key in query_params) {
  //       loop_start && (return_url += "&");
  //       return_url = return_url + key + "=" + query_params[key];
  //       loop_start = true;
  //     }
  //   }
  //   if (url_params) {
  //     for (var url_key in url_params) {
  //       return_url = return_url.replaceAll(":" + url_key, url_params[url_key]);
  //     }
  //   }
  //   return return_url;
  // },

  // post: function (
  //   url: any,
  //   url_type: any,
  //   is_header: any,
  //   data: any,
  //   query_params: any,
  //   url_params: any,
  //   is_json: any
  // ) {
  //   return new Promise((resolve, reject) => {
  //     if (is_header) {
  //       var header = this.getHeader(is_json);
  //     }
  //     let service_url = this.buildUrl(url, url_type, query_params, url_params);
  //     axios
  //       .post(service_url, data, header)
  //       .then((data) => {
  //         resolve(data.data);
  //       })
  //       .catch((er) => {
  //         reject(er);
  //       });
  //   });
  // },
    
  //   get: function (url:any, url_type:any, is_header:any, query_params:any, url_params:any,is_json: any) {
  //   return new Promise((resolve, reject) => {
  //     if (is_header) {
  //       var header = this.getHeader(is_json);
  //     }
  //     let service_url = this.buildUrl(url, url_type, query_params, url_params);
  //     axios
  //       .get(service_url, header)
  //       .then((data) => {
  //         resolve(data.data);
  //       })
  //       .catch((er) => {
  //         reject(er);
  //       });
  //   });
  // },
  // // put: function (url, is_header, data, url_params, query_params) {
  // //     return new Promise((resolve, reject) => {

  // //     })
  // // },
  // delete: function (url:any, url_type:any, is_header:any, query_params:any, url_params:any,is_json: any) {
  //   return new Promise((resolve, reject) => {
  //     if (is_header) {
  //       var header = this.getHeader(is_json);
  //     }
  //     let service_url = this.buildUrl(url, url_type, query_params, url_params);
  //     axios
  //       .delete(service_url, header)
  //       .then((data) => {
  //         resolve(data.data);
  //       })
  //       .catch((er) => {
  //         reject(er);
  //       });
  //   });
  // },
};

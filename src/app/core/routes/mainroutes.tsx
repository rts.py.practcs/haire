import { Dashboard } from "../../modules/dashboard";
import { Resumes } from "../../shared/resumes";
import { Gridview } from "../../shared/grid";
import { Listview } from "../../shared/list";
import { Users } from "../../shared/users";
import { Login } from "../../modules/login";
import { Landing } from "../../shared/landing";
import { Home } from "../../shared/home";
import { Accessment } from "../../shared/accessment";

const MainRoutes = {
    path: '/',
    element: <Landing />,
    children: [
        {
            path: '/',
            element: <Login />
        },
        {
            path: 'login',
            element: <Login />
        },
        {
            path: 'home',
            element: <Home />,
            children: [
                {
                    path: '',
                    element: <Dashboard />
                },
                {
                    path: 'dashboard',
                    element: <Dashboard />
                },
                {
                    path: 'users',
                    element: <Users />
                },
                {
                    path: 'resumes',
                    element: <Resumes />,
                    children: [
                        {
                            path: '',
                            element: <Gridview />
                        },
                        {
                            path: 'grid',
                            element: <Gridview />
                        },
                        {
                            path: 'list',
                            element: <Listview />
                        }
                    ]
                },
                {
                    path: 'accessment',
                    element: <Accessment />
                }
            ]
        }
    ]
}
export default MainRoutes;

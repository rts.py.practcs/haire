import React from 'react';
import './App.scss';
import Routes from './core/routes/index'

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
